from dataclasses import dataclass


@dataclass
class ArtiConfig:
    id: str
    name: str
    spacy_id: str


arti_configs = {
    "des": ArtiConfig(id="des", name="german small", spacy_id="de_core_news_sm"),
    "dem": ArtiConfig(id="dem", name="german medium", spacy_id="de_core_news_md")
}
arti_configs_small = {
    "des": ArtiConfig(id="des", name="german small", spacy_id="de_core_news_sm"),
    "dem": ArtiConfig(id="dem", name="german medium", spacy_id="de_core_news_md")
}
