FROM python:3.7

# ENV HTTPS_PROXY=http://proxy.sozvers.at:8080 \
#     HTTP_PROXY=http://proxy.sozvers.at:8080



RUN apt-get update && apt-get install -y software-properties-common
RUN apt-get install -y python3-pip

RUN pip install --upgrade pip

RUN pip install pipenv

WORKDIR /app

COPY . /app

RUN pipenv install

RUN pipenv run python -m spacy download de_core_news_md
RUN pipenv run python -m spacy download de_core_news_sm

ENV PYTHONPATH "${PYTONPATH}:/app:/app/sever"

EXPOSE 8000

CMD pipenv run python server/server.py 8000


