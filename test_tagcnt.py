import tagcnt as tc


def test_empty():
    ling_model = tc.empty(3)
    assert len(ling_model) == 4


def test_empty_10():
    ling_model = tc.empty(10)
    assert len(ling_model) == 11
    assert len(ling_model[10]) == 10


def test_empty_5():
    ling_model = tc.empty(5)
    assert len(ling_model) == 6
    assert len(ling_model[5]) == 5
    assert len(ling_model[4]) == 4


def test_empty_3():
    ling_model = tc.empty(3)
    assert len(ling_model) == 4
    assert len(ling_model[3]) == 3
    assert ling_model[3][0] == {}
    assert ling_model[3][1] == {}
    assert ling_model[3][2] == {}


def test_add_tag():
    ling_model = tc.empty(3)
    tc.add_tag(ling_model, 2, 0, "A")
    assert ling_model[2][0].get("A") == 1


def test_normal_01():
    ling_model = tc.empty(3)
    tc.add_tag(ling_model, 2, 0, "A")
    tc.add_tag(ling_model, 2, 0, "A")
    tc.add_tag(ling_model, 2, 1, "A")

    nd = tc.normal(ling_model)
    assert nd[2][0]["A"] == 1.0
    assert nd[2][1]["A"] == 0.5


def test_normal_02():
    ling_model = tc.empty(3)
    tc.add_tag(ling_model, 3, 0, "A")
    tc.add_tag(ling_model, 3, 0, "A")
    tc.add_tag(ling_model, 3, 1, "A")
    tc.add_tag(ling_model, 3, 2, "A")
    tc.add_tag(ling_model, 3, 2, "A")
    tc.add_tag(ling_model, 3, 2, "A")
    tc.add_tag(ling_model, 3, 2, "A")

    tc.add_tag(ling_model, 3, 0, "B")
    tc.add_tag(ling_model, 3, 1, "B")
    tc.add_tag(ling_model, 3, 2, "B")

    nd = tc.normal(ling_model)

    assert nd[3][0]["A"] == 0.5
    assert nd[3][1]["A"] == 0.25
    assert nd[3][2]["A"] == 1.0

    assert nd[3][0]["B"] == 1.0
    assert nd[3][1]["B"] == 1.0
    assert nd[3][2]["B"] == 1.0
