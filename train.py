from pathlib import Path
import os
import spacy
import artiphrase as ap
import pickle as pkl

from config import arti_configs, ArtiConfig

data_dir = Path('data')
if not data_dir.exists():
    print("datr dir {} does not exist".format(data_dir))
    exit(-1)


def train(cfg: ArtiConfig):
    print(f"training: {cfg}")

    spacy.load(cfg.spacy_id)
    pkl_dir = data_dir / cfg.id
    if not pkl_dir.exists():
        os.mkdir(pkl_dir)
        print("pkl dir '{}' was created".format(pkl_dir.absolute()))

    nlp = ap.load_nlp(cfg)

    for file in os.listdir(data_dir):
        if file[-3:] == 'txt':
            bname = file[:-4]
            book = ap.Book(bname, bname, cfg)

            print("creating words for {}".format(file))
            words = book.create_words_from_txt(nlp)
            print("found {} words for {}".format(len(words), file))
            pkl_word_path = pkl_dir / "{}.words.pkl".format(bname)
            with open(str(pkl_word_path), 'wb') as f:
                pkl.dump(words, f)
                print("dumped words to {}".format(book.words_pkl_path()))

            print("creating model for {}".format(file))
            model = book.create_model_from_txt(nlp)
            print("created model for {}".format(file))
            pkl_model_path = pkl_dir / "{}.model.pkl".format(bname)
            with open(str(pkl_model_path), 'wb') as f:
                pkl.dump(model, f)
                print("dumped model to {}".format(book.model_pkl_path()))
                print("------------------------------------")


for _cfg in arti_configs.values():
    train(_cfg)
