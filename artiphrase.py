import io
from concurrent.futures import Executor

import ranphrase
import tagcnt as tc
import concurrent.futures as cf
import sortedcontainers as sc
import spacy
import pickle as pkl

from spacy.tokens.span import Span
from pathlib import Path
from typing import List, Tuple, Optional, Dict

from config import ArtiConfig, arti_configs


class Book:
    data_dir = Path("data")

    def __init__(self, name: str, file_name: str, cfg: ArtiConfig):
        self.name = name
        self.file_name: str = file_name
        self._ling_model: Optional[cf.Future] = None
        self._words: Optional[cf.Future] = None
        self.cfg = cfg

    def ling_model(self, execu: Executor) -> cf.Future:
        if self._ling_model is None:
            self._ling_model = execu.submit(self._load_model)
        return self._ling_model

    def words(self, execu: Executor) -> cf.Future:
        if self._words is None:
            self._words = execu.submit(self._load_words)
        return self._words

    def txt_path(self) -> Path:
        return self.data_dir / "{}.txt".format(self.file_name)

    def model_pkl_path(self) -> Path:
        return self.data_dir / self.cfg.id / "{}.model.pkl".format(self.file_name)

    def words_pkl_path(self) -> Path:
        return self.data_dir / self.cfg.id / "{}.words.pkl".format(self.file_name)

    def _open_path_txt(self):
        return io.open(str(self.txt_path()), 'r', encoding="UTF-8")

    def _load_model(self) -> List[List[dict]]:
        with io.open(str(self.model_pkl_path()), 'rb') as file:
            return pkl.load(file)

    def _load_words(self) -> List[List[dict]]:
        with io.open(str(self.words_pkl_path()), 'rb') as file:
            return pkl.load(file)

    def create_model_from_txt(self, nlp) -> List[List[dict]]:
        print("creating linguistic model for '{}'".format(self.name))
        ling_model = tc.empty(20)
        with self._open_path_txt() as file:
            doc = nlp(file.read())
            sentences = [sent for sent in doc.sents]
            sent_len = len(sentences)
            sent_cnt = 0
            for j, s in zip(range(sent_len), sentences):
                sp: Span = s
                splen = len(sp)
                if splen >= 4:
                    sdoc = nlp(sp.text)
                    alpha_doc = zip([t.is_alpha for t in sdoc], sdoc)
                    sdoc1 = [doc for is_alpha, doc in alpha_doc if is_alpha]
                    if 4 <= len(sdoc1) <= 15:
                        for k in range(len(sdoc1)):
                            tc.add_tag(ling_model, len(sdoc1), k, sdoc1[k].tag_)
                            tc.add_tag(ling_model, len(sdoc1), k, sdoc1[k].pos_)
                            tc.add_tag(ling_model, len(sdoc1), k, sdoc1[k].dep_)
                            tc.add_tag(ling_model, len(sdoc1), k, sdoc1[k].is_stop)
                sent_cnt += 1
                if sent_cnt % 100 == 0:
                    print("analyzed {} of {} sentences".format(sent_cnt, sent_len))

        return list(tc.normal(ling_model))

    def create_words_from_txt(self, nlp) -> list:
        print("creating words for '{}'".format(self.name))
        re = set()
        with self._open_path_txt() as file:
            doc = nlp(file.read())
            sentences = [sent for sent in doc.sents]
            sent_len = len(sentences)
            sent_cnt = 0
            for j, s in zip(range(sent_len), sentences):
                sp: Span = s
                splen = len(sp)
                if splen >= 4:
                    sdoc = nlp(sp.text)
                    for token in sdoc:
                        if token.is_alpha:
                            re.add(token.text.strip())
                sent_cnt += 1
                if sent_cnt % 100 == 0:
                    print("processed {} of {} sentences for words. found {} words".format(sent_cnt, sent_len, len(re)))

        return list(re)


class Nlps:
    nlps: dict

    def __init__(self):
        self.dict = {}

    def nlp(self, cfg: ArtiConfig):
        nlp = self.dict.get(cfg.id)
        if nlp is None:
            print(f"loading {cfg.spacy_id}")
            nlp = load_nlp(cfg)
            self.dict[cfg.id] = nlp
        return nlp


class Artiphrase:

    def __init__(self, ling_model_book: Book, words_book: Book, nlp, cfg):
        self.nlp = nlp
        self.cfg = cfg
        self.rated_phrases: sc.SortedKeyList = sc.SortedKeyList(key=self._key)
        self.ling_model_book: Book = ling_model_book
        self.words_book: Book = words_book

    def update_phrases(self,
                       execu: Executor,
                       blocking: bool = True,
                       phrase_length_list: List[int] = None,
                       new_phrases_cnt: int = 100,
                       output_length: int = 20) -> List[Tuple[str, str]]:

        if phrase_length_list is None:
            phrase_length_list = [3, 4, 5]

        words_future = self.words_book.words(execu)
        ling_model_future = self.ling_model_book.ling_model(execu)

        if not blocking and (not words_future.done() or not ling_model_future.done()):
            return [("initializing", "...")]

        words: List[str] = words_future.result()
        ling_model: list = ling_model_future.result()
        i = 0
        phrases = ranphrase.ran_phrase_factory(phrase_length_list, words)
        for phrase_list in phrases:
            tup = (self._calc_rating(phrase_list, ling_model), phrase_list)
            self.rated_phrases.add(tup)
            i += 1
            if i > new_phrases_cnt:
                break
        del self.rated_phrases[output_length:]
        re = []
        for rating, phrase_list in self.rated_phrases:
            phrase_list = [w.lower() for w in phrase_list]
            phrase_str = " ".join(phrase_list)
            rating_str = "{:4.2f}".format(rating)
            re.append((rating_str, phrase_str))
        return re

    @staticmethod
    def _key(t: Tuple) -> float:
        return -t[0]

    def _calc_rating(self, pr: list, ling_model: list) -> float:
        re = 0.0

        def add_value(v, fact):
            nonlocal re
            if v is not None:
                re += v * fact

        doc = self.nlp(" ".join(pr))
        leng = len(doc)
        ling_model1 = ling_model[leng]
        for k in range(leng):
            add_value(ling_model1[k].get(doc[k].tag_), 6)
            add_value(ling_model1[k].get(doc[k].pos_), 4)
            add_value(ling_model1[k].get(doc[k].dep_), 2)
            add_value(ling_model1[k].get(doc[k].is_stop), 1)
        return re / leng


def books_dict(cfg: ArtiConfig) -> Dict[str, Book]:
    return {
        'goethe1': Book("Goethe - Italienische Reise. Teil 1", "pr_goethe_itreise1", cfg),
        'goethe2': Book("Goethe - Italienische Reise. Teil 2", "pr_goethe_itreise2", cfg),
        'kafka1': Book("Kafka - Das Urteil", "pr_kafka_das_urteil", cfg),
        'hoffm1': Book("Hoffmann - Struwwelpeter", "ly_hoffmann_struwwelpeter", cfg),
        'trakl1': Book("Trakl - Gedichte Teil 1", "ly_trakl_a", cfg),
        'trakl2': Book("Trakl - Gedichte Teil 2", "ly_trakl_b", cfg),
        'trakl3': Book("Trakl - Gedichte Teil 3", "ly_trakl_c", cfg),
    }


def load_nlp(cfg: ArtiConfig):
    return spacy.load(cfg.spacy_id)


if __name__ == "__main__":
    _cfg = arti_configs["dem"]
    _nlp = load_nlp(_cfg)
    _execu: Executor = cf.ThreadPoolExecutor(max_workers=4)
    bl = books_dict(_cfg)
    mb: Book = bl['hoffm1']
    wb: Book = bl['kafka1']
    artiphrase = Artiphrase(mb, wb, _nlp, _cfg)
    print("------------------------------------------")
    print("model from {}".format(mb.name))
    print("------------------------------------------")
    print("\n".join(["{} - {}".format(a, b) for a, b in artiphrase.update_phrases(new_phrases_cnt=1000, execu=_execu)]))
