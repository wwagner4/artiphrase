import random
from typing import Iterable, List


def ran_phrase_factory(phrase_length_list: List[int], words: List[str]) -> Iterable[List[str]]:
    def gen():
        while True:
            leng = _phrase_length(phrase_length_list)
            yield _ran_phrase(leng, words)

    return gen()


def _phrase_length(len_list: List[int]) -> int:
    i = random.randint(0, len(len_list) - 1)
    return len_list[i]


def _ran_word(words: list) -> str:
    i = random.randint(0, len(words) - 1)
    return words[i]


def _ran_phrase(leng: int, words: list) -> List[str]:
    return list(map(lambda i: _ran_word(words), range(leng)))
