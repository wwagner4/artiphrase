import sys
import spacy
import sortedcontainers
import pytest
import coverage

print(sys.version)
print("spacy version: {}".format(spacy.__version__))
print("pytest version: {}".format(pytest.__version__))
print("coverage version: {}".format(coverage.__version__))
print("sortedcontainers version: {}".format(sortedcontainers.__version__))
