from typing import List


def _empty(leng: int):
    return [{} for _ in range(leng)]


def empty(maxlen: int):
    return [_empty(i) for i in range(maxlen + 1)]


def add_tag(ling_model, leng: int, pos: int, key):
    def add_tag_(d: dict):
        skey = str(key)
        cnt = d.get(skey)
        if cnt is None:
            d[skey] = 1
        else:
            d[skey] += 1

    add_tag_(ling_model[leng][pos])


def normal(ling_model: List[List[dict]]) -> List:
    def _normal_dict(d: dict, max_dict: dict) -> dict:
        re = {}
        for key in d.keys():
            re[key] = d[key] / max_dict[key]

        return re

    def _normal(from_num: List[dict], max_dict: dict) -> List[dict]:
        return [_normal_dict(d, max_dict) for d in from_num]

    def maxcnt(from_num: List[dict]) -> dict:
        re = {}
        for d in from_num:
            for key in d.keys():
                reval = re.get(key)
                if reval is None or reval < d[key]:
                    re[key] = d[key]

        return re

    max_dicts: List[dict] = [maxcnt(fn) for fn in ling_model]

    return [_normal(from_num, max_dict) for from_num, max_dict in zip(ling_model, max_dicts)]
