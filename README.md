# artiphrase
Create artificial phrases for fun.

# running the app using docker

```
# clone project
git clone https://gitlab.com/wwagner4/artiphrase.git

# build base image
docker build -t artiphrase_base -f DockerfileBase .

# build server image
docker build -t wwagner4/solo:artiphrase_server -f DockerfileServer .
# run server image
docker run --rm -it -p 8888:8000 wwagner4/solo:artiphrase_server

# build test image
docker build -t artiphrase_test -f DockerfileTest .
# run test image
docker run --rm -it artiphrase_test

# html urls
http://localhost:8888/
http://37.252.189.71:8888/

# Eventually push the server image to 
docker login 
docker push wwagner4/solo:artiphrase_server
docker pull wwagner4/solo:artiphrase_server
```


