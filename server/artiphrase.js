let running = true;
let animIndex = 0;
let animList = ['|','||','|||','||||','\xa0|||','\xa0\xa0||','\xa0\xa0\xa0|', '', '', '', '', ''];

function run() {
    let nextCallServer = function () {
        if (running) {
            callServer();
        }
    };
    setInterval(nextCallServer, 3000);
    setInterval(nextAnim, 50);
}


function callServer() {
    postAjax("/data", '{}', function (data) {
        let phrases = JSON.parse(data);
        let txt = '';
        let wbook = phrases['wordsBook'];
        let lmbook = phrases['lingModelBook'];
        let cfg = phrases['config'];
        let lphrases = phrases['phrases'];

        let bib = document.getElementById("booksInfo");
        bib.innerHTML = "words: " + wbook + "<br>model: " + lmbook + "<br>configuration: " + cfg;
        let tb = document.getElementById("bodyId");
        for (let phrase of lphrases) {
            txt += '<div class="divTableRow">';
            txt += '<div class="divTableCell">' + phrase['rating'] + '</div>';
            txt += '<div class="divTableCell">' + phrase['phrase'] + '</div>';
            txt += '</div>';
        }
        tb.innerHTML = txt;
    });
}

function nextAnim() {
    if (running) {
        animIndex = (animIndex + 1) % animList.length;
        let elem = document.getElementById("animated");
        elem.innerText = animList[animIndex]
    }
}

function on_pause_start() {
    let elem = document.getElementById("command");
    if (running) {
        running = false;
        elem.innerText = "resume"
    } else {
        running = true;
        elem.innerText = "pause"
    }
}

function postAjax(url, data, success) {
    let params = typeof data == 'string' ? data : Object.keys(data).map(
        function (k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
        }
    ).join('&');

    let xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('POST', url);
    xhr.onreadystatechange = function () {
        if (xhr.readyState > 3 && xhr.status === 200) {
            success(xhr.responseText);
        }
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(params);
    return xhr;
}
