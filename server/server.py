import random
from typing import Optional

from flask import Flask
import sys
import json as js

from artiphrase import Artiphrase, Book, books_dict, Nlps

import concurrent.futures as cf

import random as ran

from config import arti_configs_small

app = Flask(__name__)
cnt: int = 0
artiphrase: Optional[Artiphrase] = None
execu = cf.ThreadPoolExecutor(max_workers=4)
nlps = Nlps()


def new_artiphrase() -> Artiphrase:
    i = random.randint(0, len(arti_configs_small) - 1)
    cfg = list(arti_configs_small.values())[i]
    books = books_dict(cfg)
    bl = list(books.values())
    mi = ran.randint(0, len(bl) - 1)
    ling_model_book: Book = bl[mi]
    wi = ran.randint(0, len(bl) - 1)
    word_book: Book = bl[wi]
    nlp = nlps.nlp(cfg)
    return Artiphrase(ling_model_book, word_book, nlp, cfg)


@app.route('/', methods=['GET'])
def index():
    global artiphrase
    artiphrase = new_artiphrase()
    with open('server/artiphrase.html', 'r') as f:
        return f.read().replace('\n', '')


@app.route('/<fnam>.<fext>', methods=['GET'])
def file(fnam, fext):
    fname = 'server/{}.{}'.format(fnam, fext)
    with open(fname, 'r') as f:
        cont = f.read()
        return cont


@app.route('/data', methods=['POST'])
def data():
    global artiphrase
    if artiphrase is None:
        artiphrase = new_artiphrase()
    li = artiphrase.update_phrases(
        blocking=False,
        phrase_length_list=[4, 5, 6, 7],
        output_length=20,
        new_phrases_cnt=80,
        execu=execu)
    phrases_list = [{'rating': r, 'phrase': p} for r, p in li]
    json = {
        'phrases': phrases_list,
        'wordsBook': artiphrase.words_book.name,
        'lingModelBook': artiphrase.ling_model_book.name,
        'config': artiphrase.cfg.name,
    }
    return js.dumps(json)


args = sys.argv
if len(args) != 2:
    raise AttributeError('usage: server <port>')

app.run(host='0.0.0.0', port=args[1])
